<?php

/**
 * @file
 */

namespace Drupal\tmgmt_spreadsheet\Plugin\tmgmt_file;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmgmt\Data;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt_file\Format\FormatInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Protection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TmgmtSpreadsheetFormatPluginBase
 *
 * @package Drupal\tmgmt_spreadsheet\Plugin
 */
abstract class TmgmtSpreadsheetFormatPluginBase implements FormatInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * Read-only cells background color
   */
  const string BACKGROUND_COLOR = 'FFFFEE';

  /**
   * @var string $format
   */
  protected string $format;

  /**
   * @var \PhpOffice\PhpSpreadsheet\Spreadsheet $spreadsheet
   */
  protected Spreadsheet $spreadsheet;

  /**
   * @var \Drupal\Core\File\FileSystemInterface $file_system;
   */
  protected FileSystemInterface $file_system;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface $messenger
   */
  protected MessengerInterface $messenger;

  /**
   * @var \Drupal\tmgmt\Data $tmgmt_data
   */
  protected Data $tmgmt_data;

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystemInterface $file_system,
                              MessengerInterface $messenger,
                              Data $tmgmt_data) {
    $this->file_system = $file_system;
    $this->messenger = $messenger;
    $this->tmgmt_data = $tmgmt_data;
    $this->setFormat($this->getClassName());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('file_system'),
      $container->get('messenger'),
      $container->get('tmgmt.data')
    );
  }

  /**
   * @param string $format
   *   Set PhpSpreadsheet-compatible format string.
   */
  protected function setFormat(string $format) : void {
    $this->format = $format;
  }

  /**
   * Get PhpSpreadsheet-compatible format string.
   *
   * @return string
   */
  protected function getFormat() : string {
    return $this->format;
  }

  /**
   * {@inheritdoc}
   */
  public function export(JobInterface $job, $conditions = []) : false|string {
    // create empty spreadsheet
    $this->spreadsheet = new Spreadsheet();
    $this->spreadsheet
      ->getProperties()
      ->setCreated(\Drupal::time()->getRequestTime());
    $this->spreadsheet
      ->getActiveSheet()
      ->getProtection()
      ->setPassword('tmgmt_spreadsheet')
      ->setSheet(true);
    // set default style
    $style = [
      'font' => [
        'bold' => TRUE,
      ],
      'alignment' => [
        'vertical' => Alignment::VERTICAL_TOP,
      ],
      'fill' => [
        'fillType' => Fill::FILL_SOLID,
        'startColor' => [
          'rgb' => self::BACKGROUND_COLOR,
        ],
      ],
    ];
    $this->spreadsheet
      ->getDefaultStyle()
      ->applyFromArray($style);
    // add header
    $header = [
      'A' => 'Key',
      'B' => 'Parent label',
      'C' => 'Label',
      'D' => 'Original text',
      'E' => 'Translated text',
    ];
    foreach ($header as $col => $text) {
      $this->spreadsheet
        ->getActiveSheet()
        ->setCellValue($col . '2', $text);
    }
    $this->spreadsheet
      ->getActiveSheet()
      ->freezePane('D3');
    $row = 3;
    foreach ($job->getItems($conditions) as $item) {
      $data = $this->tmgmt_data->filterTranslatable($item->getData());
      foreach ($data as $key => $value) {
        // key
        $coordinate = 'A' . $row;
        $this->spreadsheet
          ->getActiveSheet()
          ->setCellValue($coordinate, $item->id() . '][' . $key);
        // parent label
        $coordinate = 'B' . $row;
        $parent_label = isset($value['#parent_label'][0]) ? $value['#parent_label'][0] : '';
        $this->spreadsheet
          ->getActiveSheet()
          ->calculateColumnWidths()
          ->setCellValue($coordinate, $parent_label);
        // label
        $coordinate = 'C' . $row;
        $label = isset($value['#label']) ? $value['#label'] : '';
        $this->spreadsheet
          ->getActiveSheet()
          ->calculateColumnWidths()
          ->setCellValue($coordinate, $label);
        // original text
        $coordinate = 'D' . $row;
        $text = isset($value['#text']) ? $value['#text'] : '';
        $this->spreadsheet
          ->getActiveSheet()
          ->calculateColumnWidths()
          ->setCellValue($coordinate, $text);
        $style = [
          'font' => [
            'bold' => FALSE,
          ],
        ];
        $this->spreadsheet
          ->getActiveSheet()
          ->getStyle($coordinate)
          ->applyFromArray($style);
        // translated text
        $coordinate = 'E' . $row;
        $text = isset($value['#text']) ? $value['#text'] : '';
        $this->spreadsheet
          ->getActiveSheet()
          ->calculateColumnWidths()
          ->setCellValue($coordinate, $text)
          ->getStyle($coordinate)
          ->getProtection()
          ->setLocked(Protection::PROTECTION_UNPROTECTED);
        $style = [
          'font' => [
            'bold' => FALSE,
          ],
          'fill' => [
            'fillType' => Fill::FILL_NONE,
          ],
        ];
        $this->spreadsheet
          ->getActiveSheet()
          ->getStyle($coordinate)
          ->applyFromArray($style);
        // auto size cells
        foreach (['B', 'C', 'D', 'E'] as $index) {
          $this->spreadsheet
            ->getActiveSheet()
            ->getColumnDimension($index)
            ->setAutoSize(true);
        }
        $row++;
      }
    }
    // write some metadatas in the firts row and hide
    $this->spreadsheet
      ->getActiveSheet()
      ->setCellValue('A1', $job->id());
    $this->spreadsheet
      ->getActiveSheet()
      ->setCellValue('B1', $job->getRemoteSourceLanguage());
    $this->spreadsheet
      ->getActiveSheet()
      ->setCellValue('C1', $job->getRemoteTargetLanguage());
    $this->spreadsheet
      ->getActiveSheet()
      ->getRowDimension(1)
      ->setVisible(FALSE);
    // hide key column and select active cell
    $this->spreadsheet
      ->getActiveSheet()
      ->setSelectedCell('D3')
      ->getColumnDimension('A')
      ->setVisible(FALSE);
    $objWriter = IOFactory::createWriter($this->spreadsheet, $this->getFormat());
    ob_start();
    $objWriter->save('php://output');
    return ob_get_clean();
  }

  /**
   * {@inheritdoc}
   */
  public function import($imported_file, $is_file = TRUE) : array {
    $result = [];
    $rows = $this->spreadsheet
      ->getActiveSheet()
      ->getRowIterator(3); // skip metadata and header rows
    foreach ($rows as $index => $row) {
      $key = $this->spreadsheet
        ->getActiveSheet()
        ->getCell('A' . $index)
        ->getValue();
      $result[$key]['#text'] = $this->spreadsheet
        ->getActiveSheet()
        ->getCell('E' . $index)
        ->getValue();
    }
    return $this->tmgmt_data->unflatten($result);
  }

  /**
   * {@inheritdoc}
   */
  public function validateImport($imported_file, $is_file = TRUE) : FALSE|Job {
    $real_path = $this->file_system->realpath($imported_file);
    try {
      $file_type = IOFactory::identify($real_path);
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      return FALSE;
    }
    if ($file_type != $this->getFormat()) {
      $this->messenger->addError($this->t('Missing file format, please choose another plugin.'));
      return FALSE;
    }
    $reader = IOFactory::createReader($this->getFormat());
    $this->spreadsheet = $reader->load($real_path);
    // check tmgmt job
    $job_id = (int) $this->spreadsheet
      ->getActiveSheet()
      ->getCell('A1')
      ->getValue();
    if (($job = Job::load($job_id)) === NULL) {
      $this->messenger->addError($this->t('Internal TMGMT error, unable load job.'));
      return FALSE;
    }
    // check languages
    $source_language = (string) $this->spreadsheet
      ->getActiveSheet()
      ->getCell('B1')
      ->getValue();
    $target_language = (string) $this->spreadsheet
      ->getActiveSheet()
      ->getCell('C1')
      ->getValue();
    if ($source_language != $job->getRemoteSourceLanguage() || $target_language != $job->getRemoteTargetLanguage()) {
      $this->messenger->addError($this->t('Internal TMGMT error, missing languages.'));
      return FALSE;
    }
    return $job;
  }

  /**
   * Get short class name.
   */
  public function getClassName() : string {
    return (new \ReflectionClass($this))->getShortName();
  }
}
