<?php

/**
 * @file
 */

namespace Drupal\tmgmt_spreadsheet\Plugin\tmgmt_file\Format;

use Drupal\Core\Annotation\Translation;
use Drupal\tmgmt_file\Annotation\FormatPlugin;
use Drupal\tmgmt_spreadsheet\Plugin\tmgmt_file\TmgmtSpreadsheetFormatPluginBase;


/**
 * Class Xlsx
 *
 * @package Drupal\tmgmt_spreadsheet\Plugin\tmgmt_file\Format
 *
 * Export into Microsoft Excel XLSX-format
 *
 * @FormatPlugin(
 *   id="xlsx",
 *   label=@Translation("XLSX")
 * )
 */

class Xlsx extends TmgmtSpreadsheetFormatPluginBase {}
