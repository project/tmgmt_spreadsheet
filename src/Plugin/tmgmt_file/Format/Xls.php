<?php

/**
 * @file
 */

namespace Drupal\tmgmt_spreadsheet\Plugin\tmgmt_file\Format;

use Drupal\Core\Annotation\Translation;
use Drupal\tmgmt_file\Annotation\FormatPlugin;
use Drupal\tmgmt_spreadsheet\Plugin\tmgmt_file\TmgmtSpreadsheetFormatPluginBase;


/**
 * Class Xls
 *
 * @package Drupal\tmgmt_spreadsheet\Plugin\tmgmt_file\Format
 *
 * Export into Microsoft Excel XLS-format
 *
 * @FormatPlugin(
 *   id="xls",
 *   label=@Translation("XLS")
 * )
 */

class Xls extends TmgmtSpreadsheetFormatPluginBase {}
