TMGMT Export / Import spreadsheet
=================================


URL: https://www.drupal.org/project/tmgmt_spreadsheet/


A translator which allows you to export source data into a spreadsheet
and import the translated in return.


Required:
- Translation Management Tool (https://www.drupal.org/project/tmgmt)
- PhpSpreadsheet (https://phpspreadsheet.readthedocs.io/en/latest/)


Allowed formats:
 - Xls, Xlsx
