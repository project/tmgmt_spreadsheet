<?php

namespace Drupal\Tests\tmgmt_spreadsheet\Traits;

/**
 * Test data trait for tmgmt_spreadsheet module.
 *
 * @group tmgmt_spreadsheet
 */
trait TestDataTrait {

  /**
   * Get test data.
   */
  protected function getTestNodes() {
    return [
      [
        // nid: 1
        'title' => 'Original title 01',
        'body' => [
          'summary' => 'Original body-summary 01',
          'value' => 'Original body 01',
        ],
      ],
      [
        // nid: 2
        'title' => 'Original title 02',
        'body' => 'Original body 02',
        'field_string_textfield' => [
          'Original field_string_textfield 02-00',
          'Original field_string_textfield 02-01',
          'Original field_string_textfield 02-02',
        ],
      ],
    ];
  }
}
