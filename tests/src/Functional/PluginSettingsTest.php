<?php

declare(strict_types=1);

namespace Drupal\Tests\tmgmt_spreadsheet\Functional;

use Drupal\node\Entity\Node;
use Drupal\Tests\tmgmt_spreadsheet\Traits\TestDataTrait;
use Drupal\Tests\WebAssert;
use PHPUnit\Framework\Attributes\Group;

/**
 * Plugin settings test.
 */
#[Group('tmgmt_spreadsheet')]
class PluginSettingsTest extends TestBase {

  use TestDataTrait;

  /**
   * The assert session;
   *
   * @var \Drupal\Tests\WebAssert
   */
  protected WebAssert $assert;

  /**
   * {@inheritdoc}
   */
  function setUp() : void {
    parent::setUp();
    $this->assert = $this->assertSession();
    // Generate test contents.
    foreach ($this->getTestNodes() as $content) {
      $this->createNode($content + ['type' => 'page']);
    }
  }

  /**
   * Test available plugins.
   *
   * @return void
   */
  function testAvailablePlugins() : void {
    $node = Node::load(1);
    $translation_link = $node->toUrl('drupal:content-translation-overview')->toString();
    // Request translation.
    $this->drupalGet($translation_link);
    $edit = [
      "languages[hu]" => 'TRUE',
    ];
    $this->submitForm($edit, 'Request translation');
    // Check available plugins.
    $this->assert
      ->elementExists('css', '#edit-settings-export-format-xls');
    $this->assert
      ->elementExists('css', '#edit-settings-export-format-xlsx');
  }
}
