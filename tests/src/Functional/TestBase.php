<?php

declare(strict_types=1);

namespace Drupal\Tests\tmgmt_spreadsheet\Functional;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Site\Settings;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PHPUnit\Framework\Attributes\Group;

/**
 * Generic test base class.
 */
#[Group('tmgmt_spreadsheet')]
abstract class TestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'config_translation',
    'content_translation',
    'node',
    'tmgmt_config',
    'tmgmt_content',
    'tmgmt_spreadsheet',
  ];

  /**
   * @var \Drupal\node\Entity\NodeType $nodeType
   */
  protected NodeType $nodeType;

  /**
   * @var \Drupal\user\Entity\User $user
   */
  protected User $user;

  /**
   * Tested field identifier.
   *
   * @var string $field_id
   */
  protected string $field_id = 'field_string_textfield';

  /**
   * Tested worksheet from spreadsheet.
   *
   * @var \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet
   */
  protected Worksheet $worksheet;

  /**
   * Tested file real path.
   *
   * @var string
   */
  protected string $filepath;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Add language.
    ConfigurableLanguage::createFromLangcode('hu')->save();
    // Add content type with custom textfield.
    $this->nodeType = $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
    ]);
    $field_storage = FieldStorageConfig::create([
      'entity_type' => 'node',
      'field_name' => $this->field_id,
      'type' => 'string',
      'cardinality' => -1,
    ]);
    $field_storage->save();
    $field = [
      'field_name' => $this->field_id,
      'label' => 'String textfield',
      'entity_type' => 'node',
      'bundle' => 'page',
      'required' => TRUE,
    ];
    FieldConfig::create($field)->save();
    // Assign form settings.
    \Drupal::service('entity_display.repository')->getFormDisplay('node', 'page')
      ->setComponent($this->field_id)
      ->save();
    // Assign display settings.
    \Drupal::service('entity_display.repository')->getViewDisplay('node', 'page')
      ->setComponent($this->field_id)
      ->save();
    // Create and log in test user.
    $this->user = $this->drupalCreateUser([
      'administer content types',
      'administer content translation',
      'administer languages',
      'administer tmgmt',
      'create page content',
      'edit any page content',
      'translate configuration',
      'translate any entity',
    ]);
    $this->drupalLogin($this->user);
    // Enable content type translation.
    $this->drupalGet('/admin/config/regional/content-language');
    $edit = [
      'entity_types[node]' => TRUE,
      'settings[node][page][translatable]' => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');
  }

  /**
   * Request content entity translation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The source entity.
   * @param string $dest_langcode
   *  Translation destination language code.
   * @param string $translation_file_plugin
   *   Translation file format plugin.
   *
   * @return null|string
   *   Downloaded file path, or NULL, if download not enabled.
   */
  protected function getContentTranslationRequest(EntityInterface $entity, string $dest_langcode = 'hu', string $translation_file_plugin = 'xls') {
    if ($entity->hasLinkTemplate('drupal:content-translation-overview')) {
      $page = $this->getSession()->getPage();
      $translation_link = $entity->toUrl('drupal:content-translation-overview')->toString();
      // Request translation.
      $this->drupalGet($translation_link);
      $edit = [
        "languages[$dest_langcode]" => 'TRUE',
      ];
      $this->submitForm($edit, 'Request translation');
      $edit = [
        'settings[export_format]' => $translation_file_plugin
      ];
      $this->submitForm($edit, 'Submit to provider');
      $page->clickLink('here');
      return $this->getRealFilePathFromUrl($this->getUrl());
    }
    return NULL;
  }

  /**
   * Request configuration translation.
   *
   * @param string $path
   *   Configuration form path
   * @param string $dest_langcode
   *  Translation destination language code.
   * @param string $translation_file_plugin
   *   Translation file format plugin.
   *
   * @return null|string
   *   Downloaded file path, or NULL, if download not enabled.
   */
  protected function getConfigTranslationRequest(string $path, string $dest_langcode = 'hu', string $translation_file_plugin = 'xls') {
    $path = '/' . trim($path, '/') . '/translate';
    if (\Drupal::service('path.validator')->isValid($path)) {
      $page = $this->getSession()->getPage();
      // Request translation.
      $this->drupalGet($path);
      $edit = [
        "languages[$dest_langcode]" => 'TRUE',
      ];
      $this->submitForm($edit, 'Request translation');
      $edit = [
        'settings[export_format]' => $translation_file_plugin
      ];
      $this->submitForm($edit, 'Submit to provider');
      $page->clickLink('here');
      return $this->getRealFilePathFromUrl($this->getUrl());
    }
    return NULL;
  }

  /**
   * Get real file path from URL.
   *
   * @param string $url
   *   Given URL.
   *
   * @return string|false
   *   Real file path or FALSE on failure.
   */
  protected function getRealFilePathFromUrl(string $url) : string|false {
    // TODO: WORKAROUND?
    if ($url == '') {
      return FALSE;
    }
    $base_url = \Drupal::service('router.request_context')->getCompleteBaseUrl();
    $relative_url = str_replace($base_url, '', $url);
    $scheme_value_path = Settings::get('file_public_path');
    $filepath_with_scheme = 'public:' . str_replace($scheme_value_path, '', $relative_url);
    $real_filepath = \Drupal::service('file_system')->realpath($filepath_with_scheme);
    return $real_filepath;
  }
}
