<?php

namespace Drupal\Tests\tmgmt_spreadsheet\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for tmgmt_spreadsheet.
 *
 * @group tmgmt_spreadsheet
 */
class GenericTest extends GenericModuleTestBase {}
