<?php

declare(strict_types=1);

namespace Drupal\Tests\tmgmt_spreadsheet\Functional;

use Drupal\node\Entity\Node;
use Drupal\Tests\tmgmt_spreadsheet\Traits\TestDataTrait;
use Drupal\Tests\WebAssert;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PHPUnit\Framework\Attributes\Group;

/**
 * Configuration translation.
 */
#[Group('tmgmt_spreadsheet')]
class ConfigTranslationTest extends TestBase {

  /**
   * The assert session;
   *
   * @var \Drupal\Tests\WebAssert
   */
  protected WebAssert $assert;

  /**
   * {@inheritdoc}
   */
  function setUp() : void {
    parent::setUp();
    $this->assert = $this->assertSession();
    $this->filepath = $this->getConfigTranslationRequest('admin/config/system/site-information');
    $this->assertNotNull($this->filepath, 'Downloaded file is exist.');
    $this->worksheet = IOFactory::load($this->filepath)->getActiveSheet();
  }

  /**
   * Test cell values.
   *
   * @return void
   */
  function testCellValues() : void {
    $values = [
      'A1' => ['value' => '1', 'message' => 'Job ID'],
      'B1' => ['value' => 'en', 'message' => 'Original language code'],
      'C1' => ['value' => 'hu', 'message' => 'Translated language code'],
      'A2' => ['value' => 'Key', 'message' => 'Key column header'],
      'B2' => ['value' => 'Parent label', 'message' => 'Parent label column header'],
      'C2' => ['value' => 'Label', 'message' => 'Label column header'],
      'D2' => ['value' => 'Original text', 'message' => 'Original text column header'],
      'E2' => ['value' => 'Translated text', 'message' => 'Translated text column header'],
      // Title field
      'A3' => ['value' => '1][name', 'message' => 'Config key'],
      'B3' => ['value' => 'Site name', 'message' => 'Config parent label'],
      'C3' => ['value' => 'Site name', 'message' => 'Config label'],
      'D3' => ['value' => 'Drupal', 'message' => 'Original config value'],
      'E3' => ['value' => 'Drupal', 'message' => 'Translatable config value'],
    ];
    foreach ($values as $cell => $expected_value) {
      $value = $this->worksheet
        ->getCell($cell)
        ->getValue();
      $this->assertEquals($expected_value['value'], $value, $expected_value['message']);
    }
  }

  /**
   * Translate fields.
   *
   * @return void
   */
  function testFieldTranslate() : void {
    // Title
    $this->worksheet
      ->getCell('E3')
      ->setValue('Translated Drupal');
    // Save file.
    $objWriter = IOFactory::createWriter($this->worksheet->getParent(), 'Xls');
    $objWriter->save($this->filepath);
    // Upload file.
    $this->drupalGet('/admin/tmgmt/jobs/1');
    $edit = [
      'files[file]' => $this->filepath
    ];
    $this->submitForm($edit, 'Import');
    // Review updated translations.
    $this->drupalGet('/admin/tmgmt/items/1');
    $this->assert
      ->fieldValueEquals('name[translation]', 'Translated Drupal');
    // Complete translation.
    $this->submitForm([], 'Save as completed');
    $this->assert
      ->statusMessageContains('Validation completed successfully.');
  }
}
