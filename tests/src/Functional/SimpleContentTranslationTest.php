<?php

declare(strict_types=1);

namespace Drupal\Tests\tmgmt_spreadsheet\Functional;

use Drupal\node\Entity\Node;
use Drupal\Tests\tmgmt_spreadsheet\Traits\TestDataTrait;
use Drupal\Tests\WebAssert;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PHPUnit\Framework\Attributes\Group;

/**
 * Simple content translation.
 */
#[Group('tmgmt_spreadsheet')]
class SimpleContentTranslationTest extends TestBase {

  use TestDataTrait;

  /**
   * The assert session;
   *
   * @var \Drupal\Tests\WebAssert
   */
  protected WebAssert $assert;

  /**
   * {@inheritdoc}
   */
  function setUp() : void {
    parent::setUp();
    $this->assert = $this->assertSession();
    // Generate test contents.
    foreach ($this->getTestNodes() as $content) {
      $this->createNode($content + ['type' => 'page']);
    }
    $this->filepath = $this->getContentTranslationRequest(Node::load(1));
    $this->assertNotNull($this->filepath, 'Downloaded file is exist.');
    $this->worksheet = IOFactory::load($this->filepath)->getActiveSheet();
  }

  /**
   * Test hidden cells.
   *
   * @return void
   */
  function testHiddenCells() : void {
    $this->assertFalse($this->worksheet
      ->getRowDimension(1)
      ->getVisible(), "Index '1' row is hidden.");
    $this->assertFalse($this->worksheet
      ->getColumnDimension('A')
      ->getVisible(), "Index 'A' column is hidden.");
  }

  /**
   * Test cells protection.
   *
   * @return void
   */
  function testCellsProtection(): void {
    // Worksheet test.
    $locked = $this->worksheet
      ->getProtection()
      ->getSheet();
    $this->assertTrue($locked, 'Worksheet is locked.');
    // Protected cells.
    foreach (['A', 'B', 'C', 'D'] as $column) {
      for ($row = 1; $row <= 5; $row++) {
        $locked_cell = $this->worksheet
          ->getCell("$column$row")
          ->getStyle()
          ->getProtection()
          ->getLocked();
        $this->assertEquals('inherit', $locked_cell, "$column$row cell is locked.");
      }
    }
    // Unprotected cells.
    for ($row = 3; $row <= 5; $row++) {
      $unlocked_cell = $this->worksheet
        ->getCell("E$row")
        ->getStyle()
        ->getProtection()
        ->getLocked();
      $this->assertEquals('unprotected', $unlocked_cell, "E$row cell is unlocked.");
    }
  }

  /**
   * Test cell values.
   *
   * @return void
   */
  function testCellValues() : void {
    $values = [
      'A1' => ['value' => '1', 'message' => 'Job ID'],
      'B1' => ['value' => 'en', 'message' => 'Original language code'],
      'C1' => ['value' => 'hu', 'message' => 'Translated language code'],
      'A2' => ['value' => 'Key', 'message' => 'Key column header'],
      'B2' => ['value' => 'Parent label', 'message' => 'Parent label column header'],
      'C2' => ['value' => 'Label', 'message' => 'Label column header'],
      'D2' => ['value' => 'Original text', 'message' => 'Original text column header'],
      'E2' => ['value' => 'Translated text', 'message' => 'Translated text column header'],
      // Title field
      'A3' => ['value' => '1][title][0][value', 'message' => 'Title field key'],
      'B3' => ['value' => 'Title', 'message' => 'Title field parent label'],
      'C3' => ['value' => '', 'message' => 'Title field label'],
      'D3' => ['value' => 'Original title 01', 'message' => 'Original title field value'],
      'E3' => ['value' => 'Original title 01', 'message' => 'Translatable title field value'],
      // Body field
      'A4' => ['value' => '1][body][0][value', 'message' => 'Body field key'],
      'B4' => ['value' => 'Body', 'message' => 'Body field parent label'],
      'C4' => ['value' => 'Text', 'message' => 'Body field label'],
      'D4' => ['value' => 'Original body 01', 'message' => 'Original body field value'],
      'E4' => ['value' => 'Original body 01', 'message' => 'Translatable body field value'],
      // Body - summary field
      'A5' => ['value' => '1][body][0][summary', 'message' => 'Body summary field key'],
      'B5' => ['value' => 'Body', 'message' => 'Body summary field parent label'],
      'C5' => ['value' => 'Summary', 'message' => 'Body summary field label'],
      'D5' => ['value' => 'Original body-summary 01', 'message' => 'Original body summary field value'],
      'E5' => ['value' => 'Original body-summary 01', 'message' => 'Translatable body summary field value'],
    ];
    foreach ($values as $cell => $expected_value) {
      $value = $this->worksheet
        ->getCell($cell)
        ->getValue();
      $this->assertEquals($expected_value['value'], $value, $expected_value['message']);
    }
  }

  /**
   * Translate fields.
   *
   * @return void
   */
  function testFieldTranslate() : void {
    // Title
    $this->worksheet
      ->getCell('E3')
      ->setValue('Modified title 01');
    // Body
    $this->worksheet
      ->getCell('E4')
      ->setValue('Modified body 01');
    // Summary
    $this->worksheet
      ->getCell('E5')
      ->setValue('Modified body-summary 01');
    // Save file.
    $objWriter = IOFactory::createWriter($this->worksheet->getParent(), 'Xls');
    $objWriter->save($this->filepath);
    // Upload file.
    $this->drupalGet('/admin/tmgmt/jobs/1');
    $edit = [
      'files[file]' => $this->filepath
    ];
    $this->submitForm($edit, 'Import');
    // Review updated translations.
    $this->drupalGet('/admin/tmgmt/items/1');
    $this->assert
      ->fieldValueEquals('title|0|value[translation]', 'Modified title 01');
    $this->assert
      ->fieldValueEquals('body|0|value[translation]', 'Modified body 01');
    $this->assert
      ->fieldValueEquals('body|0|summary[translation]', 'Modified body-summary 01');
    // Complete translation.
    $this->submitForm([], 'Save as completed');
    $this->assert
      ->statusMessageContains('Validation completed successfully.');
  }
}
