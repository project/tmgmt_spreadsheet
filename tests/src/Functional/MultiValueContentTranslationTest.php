<?php

declare(strict_types=1);

namespace Drupal\Tests\tmgmt_spreadsheet\Functional;

use Drupal\node\Entity\Node;
use Drupal\Tests\tmgmt_spreadsheet\Traits\TestDataTrait;
use Drupal\Tests\WebAssert;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PHPUnit\Framework\Attributes\Group;

/**
 * Multi-value content translation.
 */
#[Group('tmgmt_spreadsheet')]
class MultiValueContentTranslationTest extends TestBase {

  use TestDataTrait;

  /**
   * The assert session;
   *
   * @var \Drupal\Tests\WebAssert
   */
  protected WebAssert $assert;

  /**
   * {@inheritdoc}
   */
  function setUp() : void {
    parent::setUp();
    $this->assert = $this->assertSession();
    // Generate test contents.
    foreach ($this->getTestNodes() as $content) {
      $this->createNode($content + ['type' => 'page']);
    }
    $this->filepath = $this->getContentTranslationRequest(Node::load(2));
    $this->assertNotNull($this->filepath, 'Downloaded file is exist.');
    $this->worksheet = IOFactory::load($this->filepath)->getActiveSheet();
  }

  /**
   * Test cell values.
   *
   * @return void
   */
  function testCellValues() : void {
    $values = [
      'A1' => ['value' => '1', 'message' => 'Job ID'],
      'B1' => ['value' => 'en', 'message' => 'Original language code'],
      'C1' => ['value' => 'hu', 'message' => 'Translated language code'],
      'A2' => ['value' => 'Key', 'message' => 'Key column header'],
      'B2' => ['value' => 'Parent label', 'message' => 'Parent label column header'],
      'C2' => ['value' => 'Label', 'message' => 'Label column header'],
      'D2' => ['value' => 'Original text', 'message' => 'Original text column header'],
      'E2' => ['value' => 'Translated text', 'message' => 'Translated text column header'],
      // Title field
      'A3' => ['value' => '1][title][0][value', 'message' => 'Title field key'],
      'B3' => ['value' => 'Title', 'message' => 'Title field parent label'],
      'C3' => ['value' => '', 'message' => 'Title field label'],
      'D3' => ['value' => 'Original title 02', 'message' => 'Original title field value'],
      'E3' => ['value' => 'Original title 02', 'message' => 'Translatable title field value'],
      // Body field
      'A4' => ['value' => '1][body][0][value', 'message' => 'Body field key'],
      'B4' => ['value' => 'Body', 'message' => 'Body field parent label'],
      'C4' => ['value' => 'Text', 'message' => 'Body field label'],
      'D4' => ['value' => 'Original body 02', 'message' => 'Original body field value'],
      'E4' => ['value' => 'Original body 02', 'message' => 'Translatable body field value'],
      // String field - delta 0
      'A5' => ['value' => '1][field_string_textfield][0][value', 'message' => 'String field key - delta 0'],
      'B5' => ['value' => 'String textfield', 'message' => 'String field parent label - delta 0'],
      'C5' => ['value' => '', 'message' => 'String field label - delta 0'],
      'D5' => ['value' => 'Original field_string_textfield 02-00', 'message' => 'Original string field value - delta 0'],
      'E5' => ['value' => 'Original field_string_textfield 02-00', 'message' => 'Translatable string field value - delta 0'],
      // String field - delta 1
      'A6' => ['value' => '1][field_string_textfield][1][value', 'message' => 'String field key - delta 1'],
      'B6' => ['value' => 'String textfield', 'message' => 'String field parent label - delta 1'],
      'C6' => ['value' => '', 'message' => 'String field label - delta 1'],
      'D6' => ['value' => 'Original field_string_textfield 02-01', 'message' => 'Original string field value - delta 1'],
      'E6' => ['value' => 'Original field_string_textfield 02-01', 'message' => 'Translatable string field value - delta 1'],
      // String field - delta 2
      'A7' => ['value' => '1][field_string_textfield][2][value', 'message' => 'String field key - delta 2'],
      'B7' => ['value' => 'String textfield', 'message' => 'String field parent label - delta 2'],
      'C7' => ['value' => '', 'message' => 'String field label - delta 2'],
      'D7' => ['value' => 'Original field_string_textfield 02-02', 'message' => 'Original string field value - delta 2'],
      'E7' => ['value' => 'Original field_string_textfield 02-02', 'message' => 'Translatable string field value - delta 2'],
    ];
    foreach ($values as $cell => $expected_value) {
      $value = $this->worksheet
        ->getCell($cell)
        ->getValue();
      $this->assertEquals($expected_value['value'], $value, $expected_value['message']);
    }
  }

  /**
   * Translate fields.
   *
   * @return void
   */
  function testFieldTranslate() : void {
    // Title
    $this->worksheet
      ->getCell('E3')
      ->setValue('Modified title 02');
    // Body
    $this->worksheet
      ->getCell('E4')
      ->setValue('Modified body 02');
    // String field - delta 0
    $this->worksheet
      ->getCell('E5')
      ->setValue('Modified field_string_textfield 02-00');
    // String field - delta 1
    $this->worksheet
      ->getCell('E6')
      ->setValue('Modified field_string_textfield 02-01');
    // String field - delta 2
    $this->worksheet
      ->getCell('E7')
      ->setValue('Modified field_string_textfield 02-02');
    // Save file.
    $objWriter = IOFactory::createWriter($this->worksheet->getParent(), 'Xls');
    $objWriter->save($this->filepath);
    // Upload file.
    $this->drupalGet('/admin/tmgmt/jobs/1');
    $edit = [
      'files[file]' => $this->filepath
    ];
    $this->submitForm($edit, 'Import');
    // Review updated translations.
    $this->drupalGet('/admin/tmgmt/items/1');
    $this->assert
      ->fieldValueEquals('title|0|value[translation]', 'Modified title 02');
    $this->assert
      ->fieldValueEquals('body|0|value[translation]', 'Modified body 02');
    $this->assert
      ->fieldValueEquals('field_string_textfield|0|value[translation]', 'Modified field_string_textfield 02-00');
    $this->assert
      ->fieldValueEquals('field_string_textfield|1|value[translation]', 'Modified field_string_textfield 02-01');
    $this->assert
      ->fieldValueEquals('field_string_textfield|2|value[translation]', 'Modified field_string_textfield 02-02');
    // Complete translation.
    $this->submitForm([], 'Save as completed');
    $this->assert
      ->statusMessageContains('Validation completed successfully.');
  }
}
